package notification

import (
	"bytes"
	"context"
	"fmt"
	strip "github.com/grokify/html-strip-tags-go"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"net/http"
)
const firebaseScope = "https://www.googleapis.com/auth/firebase.messaging"

func SendNotification(deviceToken string, messageTitle string, messageBody string, urlRequest string , urlRedirect string , urlKeyFile string) (err error){
	client := &http.Client{}
	messageBody = strip.StripTags(messageBody)
	bearerToken , err := getToken(urlKeyFile)
	if err != nil {
		return
	}

	//urlReq := "https://fcm.googleapis.com/v1/projects/befa-helpdesk/messages:send"
	data :=  []byte(fmt.Sprintf(`
	{
		"message": {
		"token":"%v",
		  "notification": {
			"title": "%v",
			"body": "%v"
		  },
		  "data": {
			"title": "%v",
			"body": "%v",
			"url": "%v",
			"click_action": "FLUTTER_NOTIFICATION_CLICK"
		  },
		  "webpush": {
			"headers": {
			  "Urgency": "high"
			}
		  },
		  "android": {
			"priority": "high"
		  },
		  "apns": {
			 "headers": {
				"apns-priority": "10"
			  }
		  }
		}
		}
	`, deviceToken, messageTitle, messageBody, messageTitle, messageBody, urlRedirect))

	req, err := http.NewRequest("POST", urlRequest, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", bearerToken))
	if err != nil {
		return
	}

	response, err := client.Do(req)
	if err != nil {
		return
	}

	defer response.Body.Close()
	return
}

func getToken(urlFile string) (token string, err error) {
	jsonKey, err := loadKeyFile(urlFile)
	if err != nil {
		return "" , err
	}

	cfg, err := google.JWTConfigFromJSON(jsonKey, firebaseScope)
	if err != nil {
		return "" , err
	}

	ts := cfg.TokenSource(context.Background())
	tokens, err := ts.Token()

	if err != nil {
		return "" , err
	}
	return tokens.AccessToken , err
}

func loadKeyFile(urlFile string) (body []byte, err error ){
	response, err := http.Get(urlFile)
	if err != nil {
		return
	}

	defer response.Body.Close()

	body, err = ioutil.ReadAll(response.Body)

	if err != nil {
		return
	}

	return
}
