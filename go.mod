module gitlab.com/ptami_lib/notification

go 1.15

require (
	github.com/grokify/html-strip-tags-go v0.0.1
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
)
